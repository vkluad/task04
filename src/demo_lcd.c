#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "ili9341.h"
#include "fonts.h"
#include <time.h>
#include <string.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define GPIO_PIN_RESET	LCD_PIN_RESET
#define GPIO_PIN_DC		LCD_PIN_DC
#define GPIO_PIN_CS		LCD_PIN_CS
#define GPIO_DIR		"/sys/class/gpio"

#define BUF_SIZE		64

static const char *device = "/dev/spidev0.0";
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint32_t speed = 5000000;
static uint16_t delay = 0;

#define DATA_SIZE	1000
uint16_t frame_buffer[LCD_WIDTH * LCD_HEIGHT];
int fd;

static int gpio_init(unsigned int gpio, char* direction) {
	int fd, len;
	char buf[BUF_SIZE];

	fd = open (GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror ("gpio/export");
		return fd;
	}

	len = snprintf (buf, sizeof(buf), "%d", gpio);
	write (fd, buf, len);

	snprintf (buf, sizeof(buf), GPIO_DIR "/gpio%d/direction", gpio);

	fd = open (buf, O_WRONLY);
	if (fd < 0) {
		perror ("gpio/direction");
		return fd;
 	}

	write (fd, direction, sizeof(direction));
	close (fd);

	return 0;
}

static int gpio_free(unsigned int gpio) {
	int fd, len;
 	char buf[BUF_SIZE];

	fd = open (GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror ("gpio/unexport");
		return fd;
	}

	len = snprintf (buf, sizeof(buf), "%d", gpio);
	write (fd, buf, len);
 	close (fd);
	
	return 0;
}

static int gpio_set_value(unsigned int gpio, unsigned int value) {
	int fd;
	char buf[BUF_SIZE];

	snprintf (buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio);

	fd = open (buf, O_WRONLY);
	if (fd < 0) {
		perror ("gpio/set-value");
		return fd;
	}

	if (value) {
		write (fd, "1", 2);
	}
	else {
		write (fd, "0", 2);
	}

	close (fd);
 	return 0;
}


static int gpio_get_value(unsigned int gpio) {
	int fd;
	char buf[BUF_SIZE];
	char value[2];
	snprintf (buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio);

	fd = open (buf, O_RDONLY);

	read(fd,value,2);
	int i = atoi(value);
//	printf("%d\n", i);
	close (fd);
 	if(i)
		return 0;
	else
		return 1;
}



static void lcd_reset(void) {

	gpio_set_value(GPIO_PIN_RESET, 0);
	usleep(5000);
	gpio_set_value(GPIO_PIN_RESET, 1);
	gpio_set_value(GPIO_PIN_CS, 0);
}

static void spi_write(uint8_t *tx_buffer, uint16_t len) {
	int ret;

	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx_buffer,
		.rx_buf = (unsigned long)NULL,
		.len = len,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1)
		printf("can't send spi message\n");
}

static void lcd_write_command(uint8_t cmd)
{
	gpio_set_value(GPIO_PIN_DC, 0);
	spi_write(&cmd, sizeof(cmd));
}

static void lcd_write_data(uint8_t *buff, unsigned long buff_size)
{
	unsigned long i = 0;
	
	gpio_set_value(LCD_PIN_DC, 1);
	while (buff_size > DATA_SIZE) {
		spi_write(buff + i, DATA_SIZE);
		i += DATA_SIZE;
		buff_size -= DATA_SIZE;
	}
	spi_write(buff + i, buff_size);
}

void lcd_init_ili9341(void)
{
	// SOFTWARE RESET
	lcd_write_command(0x01);
	sleep(1);

	// POWER CONTROL A
	lcd_write_command(0xCB);
	{
		uint8_t data[] = { 0x39, 0x2C, 0x00, 0x34, 0x02 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL B
	lcd_write_command(0xCF);
	{
		uint8_t data[] = { 0x00, 0xC1, 0x30 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL A
	lcd_write_command(0xE8);
	{
		uint8_t data[] = { 0x85, 0x00, 0x78 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL B
	lcd_write_command(0xEA);
	{
		uint8_t data[] = { 0x00, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER ON SEQUENCE CONTROL
	lcd_write_command(0xED);
	{
		uint8_t data[] = { 0x64, 0x03, 0x12, 0x81 };
		lcd_write_data(data, sizeof(data));
	}

	// PUMP RATIO CONTROL
	lcd_write_command(0xF7);
	{
		uint8_t data[] = { 0x20 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,VRH[5:0]
	lcd_write_command(0xC0);
	{
		uint8_t data[] = { 0x23 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,SAP[2:0];BT[3:0]
	lcd_write_command(0xC1);
	{
		uint8_t data[] = { 0x10 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL
	lcd_write_command(0xC5);
	{
		uint8_t data[] = { 0x3E, 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL 2
	lcd_write_command(0xC7);
	{
		uint8_t data[] = { 0x86 };
		lcd_write_data(data, sizeof(data));
	}

	// PIXEL FORMAT
	lcd_write_command(0x3A);
	{
		uint8_t data[] = { 0x55 };
		lcd_write_data(data, sizeof(data));
	}
	
	// FRAME RATIO CONTROL, STANDARD RGB COLOR
	lcd_write_command(0xB1);
	{
		uint8_t data[] = { 0x00, 0x18 };
		lcd_write_data(data, sizeof(data));
	}
	
	// DISPLAY FUNCTION CONTROL
	lcd_write_command(0xB6);
	{
		uint8_t data[] = { 0x08, 0x82, 0x27 };
		lcd_write_data(data, sizeof(data));
	}

	// 3GAMMA FUNCTION DISABLE
	lcd_write_command(0xF2);
	{
		uint8_t data[] = { 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// GAMMA CURVE SELECTED
	lcd_write_command(0x26);
	{
		uint8_t data[] = { 0x01 };
		lcd_write_data(data, sizeof(data));
	}
	
	// POSITIVE GAMMA CORRECTION
	lcd_write_command(0xE0);
	{
		uint8_t data[] = { 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1,
                           0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00 };
		lcd_write_data(data, sizeof(data));
	}
	
	// NEGATIVE GAMMA CORRECTION
	lcd_write_command(0xE1);
	{
		uint8_t data[] = { 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1,
                           0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F };
		lcd_write_data(data, sizeof(data));
	}

	// EXIT SLEEP
	lcd_write_command(0x11);
	usleep(12000);
    
	// TURN ON DISPLAY
	lcd_write_command(0x29);

	// MEMORY ACCESS CONTROL
	lcd_write_command(0x36);
	{
		uint8_t data[] = { 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// INVERSION
	//lcd_write_command(0x21);
}

static void lcd_set_address_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{

	lcd_write_command(LCD_CASET);
	{
		uint8_t data[] = { (x0 >> 8) & 0xFF, x0 & 0xFF,
				 (x1 >> 8) & 0xFF, x1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RASET);
	{
		uint8_t data[] = { (y0 >> 8) & 0xFF, y0 & 0xFF,
				(y1 >> 8) & 0xFF, y1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RAMWR);
}

static void lcd_update_screen(void)
{
	lcd_write_data((uint8_t*)frame_buffer, sizeof(uint16_t) * LCD_WIDTH * LCD_HEIGHT);
}

void lcd_draw_pixel(uint16_t x, uint16_t y, uint16_t color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
        
	frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
	lcd_update_screen();
}

void lcd_fill_rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
	uint16_t i;
	uint16_t j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			frame_buffer[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
		}
	}
	lcd_update_screen();
}

void lcd_fill_screen(uint16_t color)
{
	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, color);
}

static void lcd_put_char(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor) 
{
	uint32_t i, b, j;

	for (i = 0; i < font.height; i++) {
		b = font.data[(ch - 32) * font.height + i];
		for (j = 0; j < font.width; j++) {
			if ((b << j) & 0x8000)  {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(color >> 8) | (color << 8);
			} 
			else {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(bgcolor >> 8) | (bgcolor << 8);
			}
		}
	}
}
static void lcd_put_string(uint16_t xs, uint16_t ys, char* string, FontDef font, uint16_t color, uint16_t bgcolor)
{
	uint16_t x = xs, y = ys;
	for(char *str=string; *str; ++str){
		lcd_put_char(x, y, *str, font, color, bgcolor);
		x += font.width;
		if ( x >= LCD_WIDTH ){
			y += font.height;
		}
	}
}

void reverse(char s[])
 {
     int i, j;
     char c;

     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

void itoa(int n, char s[])
 {
     int i, sign;

     if ((sign = n) < 0)
         n = -n;
     i = 0;
     do {
         s[i++] = n % 10 + '0';
     } while ((n /= 10) > 0);
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

#define press_button(action,count,button)\
	{\
		static int old_press;\
		const int press = gpio_get_value(button);\
	        if(press == 1){\
			if(press != old_press){\
				old_press = 1;\
				(action)count;\
	                }\
	        }else{\
	                old_press = 0;\
	        }\
	}

void get_ip(char ip_str[]) {
	FILE *ip = popen("wget -qO - eth0.me","r");
	if ( ip != NULL ) {
		fscanf(ip,"%s",ip_str);
		printf("%s\n",ip_str);
	}
	pclose(ip);
}


int main(int argc, char *argv[]) {
	int ret = 0;
	time_t s_time;
	struct tm *time_now;
	char time_str[10] = "";
        int counter = 0, old_counter = 0;
        char s_counter[10] = "";


	gpio_init(GPIO_PIN_RESET,"out");
	gpio_init(GPIO_PIN_DC, "out");
	gpio_init(GPIO_PIN_CS, "out");
	gpio_init(LCD_BUTTON_ADD, "in");
	gpio_init(LCD_BUTTON_SUB, "in");
	gpio_init(LCD_BUTTON_COUNTER_RS, "in");

	lcd_reset();

	fd = open(device, O_RDWR);
	if (fd < 0)
		printf("can't open device\n");

	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		printf("can't set spi mode\n");
	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		printf("can't get spi mode\n");

	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word\n");
	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word\n");

	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz\n");
	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz\n");

	lcd_init_ili9341();
	printf("LCD init ok\n");
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);

	lcd_fill_screen(COLOR_BLACK);

	while(counter >= 0){

	s_time = time(NULL);
	time_now = localtime(&s_time);
	char time_str[10] = "";

	char tm_hour_s[3];
	itoa(time_now->tm_hour, tm_hour_s);
	if (strlen(tm_hour_s) < 2)
		strcat(time_str, "0");
	strcat(time_str, tm_hour_s);
	strcat(time_str, ":");

	char tm_min_s[3];
	itoa(time_now->tm_min, tm_min_s);
	if (strlen(tm_min_s) < 2)
		strcat(time_str, "0");
	strcat(time_str, tm_min_s);
	strcat(time_str, ":");

	char tm_sec_s[3];

	itoa(time_now->tm_sec,tm_sec_s);

	if (strlen(tm_sec_s) < 2)
		strcat(time_str, "0");
	strcat(time_str, tm_sec_s);

	lcd_put_string(10, 10, time_str , Font_11x18, COLOR_WHITE,COLOR_BLACK);

	press_button(counter,++,LCD_BUTTON_ADD);
	press_button(counter,--,LCD_BUTTON_SUB);

	{
		static int old_press;
		static time_t timer;
		const int press = gpio_get_value(LCD_BUTTON_COUNTER_RS);

	        if(press == 1){
			if(press != old_press){
				timer = time(NULL);
				old_press = 1;
				counter = 0;
			//	lcd_fill_screen(COLOR_BLACK);
	                }
			if((time(NULL)-2) >= timer)
				counter=-1;
			//	lcd_fill_screen(COLOR_BLACK);
	        }else{
	                old_press = 0;
	        }

	}


	itoa(counter, s_counter);
	for(int i = old_counter; i>0; i = i/10)
		strcat(s_counter, " ");
	old_counter = counter;

	lcd_put_string(LCD_WIDTH/2, LCD_HEIGHT/2, s_counter, Font_11x18, COLOR_WHITE, COLOR_BLACK);
	{
	static int counter = 100;
	if( (counter < 100) )
		counter++;
	else{
		char ip[16] = "";
		get_ip(ip);
		printf("ip: %s\n",ip);
		lcd_put_string(10 ,LCD_HEIGHT-20, ip, Font_11x18, COLOR_WHITE, COLOR_BLACK);
		counter = 0;
	}
	}
	lcd_update_screen();
	}

	lcd_write_command(0x28);

	close(fd);

	gpio_free(GPIO_PIN_RESET);
	gpio_free(GPIO_PIN_DC);
	gpio_free(GPIO_PIN_CS);
	gpio_free(LCD_BUTTON_ADD);
	gpio_free(LCD_BUTTON_SUB);
	gpio_free(LCD_BUTTON_COUNTER_RS);

	return EXIT_SUCCESS;
}
