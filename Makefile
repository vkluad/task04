.PHONY: clean all run

clean: bin
	rm -rf bin

all: bin bin/fonts.o bin/demo.o
	gcc bin/fonts.o bin/demo.o -o bin/main

bin/fonts.o: src/fonts.c src/fonts.h
	gcc -c src/fonts.c -o bin/fonts.o

bin/demo.o: src/demo_lcd.c
	gcc -c src/demo_lcd.c -o bin/demo.o

bin:
	mkdir bin

run: all
	./pullup.py
	sudo ./bin/main

debug: bin bin/fonts.o bin/demo.o
	gcc -g bin/fonts.o bin/demo.o -o bin/main
